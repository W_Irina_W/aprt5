<!DOCTYPE html>
<html lang="ru">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Задание 5</title>
    <link rel="stylesheet" href="style.css">
    <script src="script.js"></script>
    <style>
.error {
  border: 2px solid red;
}
.error_text {
  margin: 4px;
  text-decoration-line: underline;
  text-decoration-color: red;
}
    </style>
  </head>
  <body>
      <main>
        <div>
               <form action="" method="POST">
    <label>Ваше имя</label>
    <div>
    <input name="fio"  type="text" <?php if ($errors['fio']) {print 'class="error"';}?> value="<?php print $values['fio'];?>">
    </div>
    <br>
    
    <label>Ваш email</label>
    <input name="email" type="text" <?php if ($errors['email']) {print 'class="error"';}?> value="<?php print $values['email'];?>">
    <br>
    
    <p>Год рождения</p>
    <select name="year">
    <?php for($i = 1900; $i < 2022; $i++) {?>
      <option value="<?php print $i; ?>"<?= $i == $values['year'] ? 'selected' : ""?>><?= $i;?></option>
      <?php }?>
      <?php if ($errors['year']) {print 'class="error"';}?>
    </select>
    <br>
    
    <p>Пол</p>
    <label class="radio">
      <input type="radio" name="pol" value="0" checked <?php if($_COOKIE['pol_value']){ echo 'checked="checked"';}?>>
      Мужской
    </label>
    <label class="radio">
      <input type="radio" name="pol" value="1" <?php if($_COOKIE['pol_value']){ echo 'checked="checked"';}?>>
      Женский
    </label>
    <br>
    
    <p>Количество конечностей</p>
    <label class="radio">
      <input type="radio" name="konc" value="1" checked <?php echo $values['konc'] == "1" ? 'checked="checked"' :""?>>
      1
    </label>
    <label class="radio">
      <input type="radio" name="konc" value="2" <?php echo $values['konc'] == "2" ? 'checked="checked"' :""?>>
      2
    </label>
    <label class="radio">
      <input type="radio" name="konc" value="3" <?php echo $values['konc'] == "3" ? 'checked="checked"' :""?>>
      3
    </label>
    <label class="radio">
      <input type="radio" name="konc" value="4" <?php echo $values['konc'] == "4" ? 'checked="checked"' :""?>>
      4
    </label>
    <br>
    <br>
    
    <select name="abilities[]" multiple <?php if ($errors['abilities']) {print 'class="error"';}?>>
      <?php 
      foreach ($abilities as $key => $value) {
        $selected = !empty($values['abilities'][$key]) ? "" : 'selected="selected"';
        printf('<option value="%s"%s>%s</option>', $key, $selected, $value);
      }
      ?>
    </select>
    <br>
    
    <p>Биография</p>
    <textarea name="biography" placeholder="Биография" rows=10 cols=30 <?php if ($errors['biography']) {print 'class="error"';}?>><?php print $values['biography'];?></textarea>
    <br>
    
    <input type="checkbox" name="checkbox" <?php if ($errors['checkbox']) {print 'class="error"';}?> <?= $values['checkbox'] == "on" ? 'checked="checked"' : "";?>>Поставьте галочку
    <br>
    <input type="submit" value="Отправить">
  </form>
          </div>
              <a href="login.php">logout/login</a>
      </main>
      <?php


if (!empty($messages)) {
print $messages[8];
print $messages[9];
if(!empty($_SESSION['login']))
printf($messages[10], $_SESSION['login'], $_SESSION['uid']);
}
?>
  </body>

</html>
